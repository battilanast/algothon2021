import numpy as np
import pandas as pd
from time import time
import math

from typing import List

from stockstats import StockDataFrame as Sdf

TECHNICAL_INDICATORS_LIST = ["macd", "boll_ub", "boll_lb", "rsi_30", "cci_30", "dx_30", "close_30_sma", "close_60_sma"]


def load_given_data(filename: str) -> pd.DataFrame:
    """
    Loads the csv file that can be found at filename
    :param filename: string containing the path to the csv file
    :return: pandas dataframe containing the loaded data
    """
    data_raw = pd.read_csv(filename)
    data_raw['date'] = (pd.to_datetime(data_raw['Position_Open'])).dt.date
    data = data_raw.drop(['Position_Open', 'Position_Close'], axis=1)

    return data


def custom_fillna(merged_input: pd.DataFrame, dataset_type: str) -> pd.DataFrame:
    """
    Computes the dot product using numpy einsum
    :param merged_input: pandas dataframe
    :param dataset_type: string declaring the dataset type, e.g. bond, commodity, currency, stock
    :return: returns a forward filled pandas dataframe
    """
    merged = merged_input.copy()
    nans = merged[merged.isnull().any(axis=1)]
    drop_idx = []
    for idx in nans.index:
        if math.isnan(merged.loc[idx].open):
            previous_date = merged.loc[idx - 1].date
            ffill = merged[merged.date == previous_date].copy()
            ffill.loc[:, 'date'] = merged.loc[idx].date
            ffill.loc[:, 'Return'] = merged.loc[idx].Return
            drop_idx.append(idx)
            merged = merged.append(ffill, ignore_index=True)

        if math.isnan(merged.loc[idx].Return):
            merged.loc[idx, 'Return'] = 0.0
        if not isinstance(merged.loc[idx].Market, str):
            merged.loc[idx, 'Market'] = dataset_type

    merged = merged.drop(drop_idx, axis=0)
    merged = merged.sort_values('date', ignore_index=True)
    return merged


class FeatureEngineer(object):
    """
    Provides methods for preprocessing the stock price data

    Attributes
    ----------
        use_technical_indicator: boolean
            we technical indicator or not
        tech_indicator_list: list
            a list of technical indicator names (modified from config.py)
        use_turbulence: boolean
            use turbulence index or not
        user_defined_feature: boolean
            user user defined features or not

    Methods
    -------
        preprocess_data()
            Main method to do the feature engineering
        add_technical_indicator()
            Calcualte technical indicators using stockstats package
        add_user_defined_feature()
            Add user defined features
        add_turbulence()
            Add turbulence index from a precalcualted dataframe
        calculate_turbulence()
            Calculate turbulence index
    """

    def __init__(self, use_technical_indicator: bool = True,
                 tech_indicator_list: List[str] = None,
                 use_turbulence: bool = False,
                 user_defined_feature: bool = False
                 ) -> None:
        if tech_indicator_list is None:
            tech_indicator_list = TECHNICAL_INDICATORS_LIST
        self.use_technical_indicator = use_technical_indicator
        self.tech_indicator_list = tech_indicator_list
        self.use_turbulence = use_turbulence
        self.user_defined_feature = user_defined_feature

    def preprocess_data(self, df: pd.DataFrame, verbose: bool = False) -> pd.DataFrame:
        """
        Main method to do the feature engineering
        @:param df: source dataframe
        @:return: a dataframe containing technical, turbulence and user defined indicator features
        """
        if self.use_technical_indicator:
            if verbose:
                print('start computing the technical indicators.....')
                start_tech = time()
                # add technical indicators using stockstats
            df = self.add_technical_indicator(df, verbose)
            if verbose:
                print("Successfully added technical indicators, elapsed time: {}".format(time() - start_tech))

                # add turbulence index for multiple stock
        if self.use_turbulence:
            if verbose:
                print('start computing the turbulence indicator.....')
            start_turb = time()
            df = self.add_turbulence(df, verbose=verbose)
            df = df.rename(columns={0: 'turbulence'})
            if verbose:
                print("Successfully added turbulence index, elapsed time: {}".format(time() - start_turb))

                # add user defined feature
        if self.user_defined_feature:
            if verbose:
                print('start computing user defined features.....')
            start_user_def = time()
            df = self.add_user_defined_feature(df)
            if verbose:
                print("Successfully added user defined features, elapsed time: {}".format(time() - start_user_def))

                # fill the missing values at the beginning and the end
        df = df.fillna(method="bfill").fillna(method="ffill")
        return df

    def add_technical_indicator(self, data: pd.DataFrame, verbose: bool = False) -> pd.DataFrame:
        """
        Calcualte technical indicators using stockstats package
        :param data: pandas dataframe
        :param verbose: print computation time of each indicator
        :return: pandas dataframe containing the technical indicators defined in self.tech_indicator_list
        """
        df = data.copy()
        stock = Sdf.retype(df.copy())
        unique_ticker = stock.tic.unique()

        for indicator in self.tech_indicator_list:
            indicator_df = pd.DataFrame()
            if verbose:
                print('Start computing {}.....'.format(indicator))
                start = time()
            for i in range(len(unique_ticker)):
                try:
                    ### Note, coputing the cci_30 is really slow, i.e. it takes about 20min to compute
                    temp_indicator = stock[stock.tic == unique_ticker[i]][indicator]
                    temp_indicator = pd.DataFrame(temp_indicator)
                    indicator_df = indicator_df.append(
                        temp_indicator, ignore_index=True
                    )
                except Exception as e:
                    print(e)
            df[indicator] = indicator_df
            if verbose:
                print('Successfully computed {}, elapsed: {}'.format(indicator, time() - start))
        return df

    @staticmethod
    def add_user_defined_feature(data: pd.DataFrame) -> pd.DataFrame:
        """
        Add user defined features
        :param data: pandas dataframe
        :return: pandas dataframe containing the user defined features
        """
        df = data.copy()
        df["daily_return"] = df.close.pct_change(1)
        # df['return_lag_1']=df.close.pct_change(2)
        # df['return_lag_2']=df.close.pct_change(3)
        # df['return_lag_3']=df.close.pct_change(4)
        # df['return_lag_4']=df.close.pct_change(5)
        return df

    def add_turbulence(self, data: pd.DataFrame, verbose: bool = False) -> pd.DataFrame:
        """
        Add turbulence index from a precalcualted dataframe
        :param data: pandas dataframe
        :param verbose: print computation time of the turbulence indicator
        :return: pandas dataframe containing the turbulence indicator
        """
        df = data.copy()
        turbulence_index = self.calculate_turbulence(df, verbose=verbose)
        df = df.merge(turbulence_index, on="date")
        df = df.sort_values(["date", "tic"]).reset_index(drop=True)
        return df

    def calculate_turbulence(self, data: pd.DataFrame, window_size: int = 252, verbose: bool = False) -> pd.DataFrame:
        """
        Calculate turbulence index
        (Source: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1691756)
        :param data: pandas dataframe
        :param window_size: defines the size of the rolling window
        :param verbose: print computation time of the turbulence indicator
        :return: pandas dataframe containing the turbulence indicator
        """
        # can add other market assets
        df = data.copy()
        if verbose:
            print('Start computing the price pivot.....')
        start = time()
        df_price_pivot = df.pivot(index="date", columns="tic", values="close")
        if verbose:
            print('df_price_pivot.shape: {}'.format(df_price_pivot.shape))
            print('Successfully computed the price pivot, elapsed: {}'.format(time() - start))
            # use returns to calculate turbulence
        if verbose:
            print('Start computing pivot pct change.....')
        start = time()
        df_price_pivot = df_price_pivot.pct_change()
        if verbose:
            print('Successfully computed the price pivot, elapsed: {}'.format(time() - start))

        # vectorised version
        filtered_pivot = df_price_pivot.fillna(0.0)
        df_pivot_rolling = filtered_pivot.rolling(window_size)
        if verbose:
            print('Start computing rolling mean.....')
            start = time()
        df_pivot_rolling_mean = df_pivot_rolling.mean().fillna(0.0)
        if verbose:
            print('Successfully computed the rolling mean, elapsed {}'.format(time() - start))
            start = time()
            print('Start computing the rolling cov.....')
        df_pivot_rolling_cov = df_pivot_rolling.cov().fillna(0.0)
        if verbose:
            print('Successfully computed the rolling cov, elapsed: {}'.format(time() - start))
            start = time()
            print('Start computing the cov invertion.....')
        df_pivot_rolling_inverted_cov = df_pivot_rolling_cov.groupby(level=0).apply(self._invert_cov)
        if verbose:
            print('Successfully computed the cov invertion, elapsed: {}'.format(time() - start))
            start = time()
            print('Start computing pivot - mean.....')
        df_pivot_price_minus_rolling_mean = filtered_pivot - df_pivot_rolling_mean
        if verbose:
            print('Successfully computed pivot - mean, elapsed: {}'.format(time() - start))
            start = time()
            print('Start computing df_pivot_price_minus_rolling_mean_T.....')
        df_pivot_price_minus_rolling_mean_T = df_pivot_price_minus_rolling_mean.unstack().to_frame().swaplevel().sort_index()
        if verbose:
            print('Successfully computed df_pivot_price_minus_rolling_mean_T, elapsed: {}'.format(time() - start))
            start = time()
            print('Start computing merged_data.....')
        df_pivot_price_minus_rolling_mean_T.columns = ['df_pivot_price_minus_rolling_mean_T']
        merged_data = df_pivot_rolling_inverted_cov.merge(df_pivot_price_minus_rolling_mean_T, left_index=True,
                                                          right_index=True)
        if verbose:
            print('Successfully merged data, elapsed: {}'.format(time() - start))
            start = time()
            print('Start computing the turbulence index.....')
        turbulence_index = merged_data.groupby(level=0).apply(self._dot_product_vectorised)
        if verbose:
            print('Successfully computed the turbulence index, elapsed: {}'.format(time() - start))

        return turbulence_index.to_frame()

    @staticmethod
    def _invert_cov(x: pd.DataFrame) -> pd.DataFrame:
        """
        Inverts the cov contained in the multiindex dataframe x
        :param x: pandas multiindex dataframe containing the covariance matrices
        :return: pandas multiindex dataframe containing inverted covariance matrices
        """
        x = x.reset_index(drop=True, level=0)
        x = pd.DataFrame(np.linalg.pinv(x.values), x.columns, x.index)
        return x

    @staticmethod
    def _dot_product_vectorised(df: pd.DataFrame) -> np.ndarray:
        """
        Computes the dot product using numpy einsum
        :param df: pandas dataframe
        :return: numpy array containing the dot product result
        """
        return np.einsum('j,jk,k->', df['df_pivot_price_minus_rolling_mean_T'], df.iloc[:, slice(-1)],
                         df['df_pivot_price_minus_rolling_mean_T'])
