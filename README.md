# Algothon Machine Learning Strategy Challenge
Aspect will provide a list of trades generated from a real production trading strategy, each trade has 3 fields: market / time-on / time-off (market is anonymised) and the pnl for each trade.  

The challenge is to use all available market information prior to the trade to forecast the profitability of the trade.


## Universe
Liquid futures on: stock indices, FX, FI, Commodities  

## Each model should predict the return of each trade and return an action (-1,0,1)
- 1 means implement the trade
- 0 means do nothing
- -1 means implement the opposite of the trade


## Evaluation criteria
The portfolio returns are calculated as the sum of returns for each trade, according to the recommended actions by the model.  

We will then score the model using the sharpe ratio of the daily returns over a test period of 2 years.


## Data sources
Any market data up to the point of trade execution. (For example, intraday price of a futures contract, different realized vol estimators, implied volatility, volume etc etc. We don't want to put any constraints here, so use whatever you think your machine learning model will perform best with - time to be creative!).  

Train period: 2006-2015 Validation period: 2016-2017 Test period: 2018-2019  

Note, I have comibined the train and validation period and let the cross-validation to take care of the correct splitting.


Source: <http://www.algothon.org/challenges.html>  
Accessed: 6/4/21



# Create a conda environment to install all the necesarry packages
If you are running macOS then the following should work:  
```conda env create --file algothon21_specs_automatically_generated.txt```

Otherwise, try using this:  
```conda env create --file algothon21_specs.yaml```


# How to run
Run the ```MST_ML_Challenge.py``` file and then you should be able to see the scores corresponding to the asset types.  
(You probably guessed it, without the provided data you will not be able to run this.)

