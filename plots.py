import pandas as pd
import os
import numpy as np
import pyfolio

from preprocessing import load_given_data

# Source: https://towardsdatascience.com/the-easiest-way-to-evaluate-the-performance-of-trading-strategies-in-python-4959fd798bb3
# Accessed: 16/4/21
# Source: https://towardsdatascience.com/calculating-sharpe-ratio-with-python-755dcb346805
# Accessed: 18/4/21


def process_returns(data):
    df = data.copy()
    df = df.fillna(0.0)
    if 'action' in df.columns:
        df['action_adj_returns'] = df.Return * df.action
    else:
        df['action_adj_returns'] = df.Return
    df.loc[df.index[0], 'action_adj_returns'] = 1.0
    df['cumulated_returns'] = df.action_adj_returns.cumsum()
    df['cumulated_returns_pct'] = df['cumulated_returns'].pct_change(1)
    df = df.fillna(0.0)
    return df


def get_returns(indicators_filename, train_filename, test_filename):
    indicators = pd.read_pickle(indicators_filename)
    indicators = indicators.set_index('date')
    indicators = indicators.drop(indicators.columns[1:], axis=1)
    returns = process_returns(indicators)

    train_returns = pd.read_pickle(train_filename)
    test_returns = pd.read_pickle(test_filename)
    returns_pred = pd.concat([train_returns, test_returns], axis=0)
    returns_pred = returns_pred.rename(columns={'return': 'Return'})
    returns_pred = process_returns(returns_pred)

    return returns, returns_pred


if __name__ == '__main__':
    asset_types = ['bond', 'commodity', 'currency', 'stock']
    indicators_filename = 'Data/ml_bond1_indicators.pkl'
    indicators = pd.read_pickle(indicators_filename)
    benchmark_df = pd.DataFrame(index=indicators.date)
    predicted_df = pd.DataFrame(index=indicators.date)
    column_names = []
    benchmark_returns = {}
    predicted_returns = {}
    for asset_type in asset_types:
        for i in range(1, 3):
            indicators_filename = os.path.join('Data', f'ml_{asset_type}{i}_indicators.pkl')
            train_filename = os.path.join('Data', f'train_{asset_type}{i}_prediction.pkl')
            test_filename = os.path.join('Data', f'test_{asset_type}{i}_prediction.pkl')
            returns, returns_pred = get_returns(indicators_filename, train_filename, test_filename)
            benchmark_returns[f'{asset_type}{i}'] = returns
            predicted_returns[f'{asset_type}{i}'] = returns_pred
            column_names.append(f'{asset_type}{i}_cumulated_returns')
            benchmark_df = benchmark_df.merge(returns.cumulated_returns, how='outer', left_index=True, right_index=True)
            predicted_df = predicted_df.merge(returns_pred.cumulated_returns, how='outer', left_index=True, right_index=True)
    benchmark_df.columns = column_names
    predicted_df.columns = column_names
    benchmark_df = benchmark_df.ffill()
    predicted_df = predicted_df.ffill()

    benchmark_df['total_returns'] = benchmark_df.sum(axis=1)
    benchmark_df['adj_total_returns'] = benchmark_df['total_returns'] / benchmark_df.iloc[0]['total_returns']
    benchmark_df['adj_total_returns_pct'] = benchmark_df['adj_total_returns'].pct_change(1).fillna(0.0)

    predicted_df['total_returns'] = predicted_df.sum(axis=1)
    predicted_df['adj_total_returns'] = predicted_df['total_returns'] / predicted_df.iloc[0]['total_returns']
    predicted_df['adj_total_returns_pct'] = predicted_df['adj_total_returns'].pct_change(1).fillna(0.0)
    print('oh hi there')
    # commodity1_indicators_filename = 'Data/ml_commodity1_indicators.pkl'
    # commodity1_train_filename = 'Data/train_commodity1_prediction.pkl'
    # commodity1_test_filename = 'Data/test_commodity1_prediction.pkl'

    # commodity1_returns, commodity1_returns_pred = get_returns(commodity1_indicators_filename, commodity1_train_filename,
    #                                                           commodity1_test_filename)
    #
    # commodity1_returns = commodity1_returns[~commodity1_returns.index.duplicated(keep='first')]
    pyfolio.create_simple_tear_sheet(predicted_df.adj_total_returns_pct,
                                     benchmark_rets=benchmark_df.adj_total_returns_pct)

    print('end of code')
