import numpy as np
import pandas as pd
from pathlib import Path
from time import time
import datetime
import os

from typing import Dict, List, Tuple

from scraper import YahooDownloader
from preprocessing import FeatureEngineer, TECHNICAL_INDICATORS_LIST, load_given_data, custom_fillna

import xgboost as xgb
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import balanced_accuracy_score, roc_auc_score, mean_squared_error
from sklearn.preprocessing import StandardScaler


### helper functions ###
def get_scores(data_dict: Dict[str, pd.DataFrame],
               pred_active_series: pd.Series,
               pred_return_series: pd.Series,
               action_pred_series: pd.Series,
               dataset_type: str,
               verbose: bool = False
               ) -> None:
    """
    Loads the csv file that can be found at filename
    :param data_dict: dictionary containing the label pandas Series
    :param pred_active_series: pandas series indicating the predicted traiding days
    :param pred_return_series: pandas series containing the predicted returns on the predicted trading days
    :param action_pred_series: pandas series containing the predicted actions on the predicted trading days
    :param dataset_type: string declaring the dataset type, e.g. bond, commodity, currency, stock
    :param verbose: print in addition the balacned accuracy, roc auc, and the mean square error score
    """
    if verbose:
        # balanced_accuracy_score
        print(
            'balanced_accuracy_score:\n{}\n'.format(balanced_accuracy_score(data_dict['y_binary'], pred_active_series)))
        # roc_auc_score
        print('roc_auc_score:\n{}\n'.format(roc_auc_score(data_dict['y_binary'], pred_active_series)))
        # mean_squared_error
        print('mean_squared_error:\n{}\n'.format(mean_squared_error(data_dict['y_reg'], pred_return_series)))

    # An oracle would make this return
    baseline = data_dict['y'].abs().sum()
    # return according to our fitted models
    prediction_score = action_pred_series.dot(data_dict['y'][pred_active_series != 0])

    print('{} baseline:\n{}'.format(dataset_type, baseline))
    print('{} prediction:\n{}'.format(dataset_type, prediction_score))


def get_binary_labels(labels: pd.Series) -> pd.Series:
    """
    Computes the binary labels 0 & 1
    :param labels: pandas series the original labels
    :return: pandas series containing the binary labels
    """
    labels_binary = labels.copy()
    labels_binary[labels_binary != 0] = 1
    labels_binary = labels_binary.astype(int)

    return labels_binary


class MLChallenge(object):
    """
    Provides methods for preprocessing the stock price data

    Attributes
    ----------
        dataset_type: str
            string declaring the dataset type, e.g. bond, commodity, currency, stock
        filename: str
            string containing path to the provided dataset
        filename_scraped_data: str
            string containing path to the scraped dataset
        filename_indicators: str
            string containing path to the computed and preprocessed indicators dataset
        filename_raw_indicators: str
            string containing path to the computed (without further preprocessing) indicators dataset
        filename_output_train: str
            string containing path and filename where the training data should be saved
        filename_output_test: str
            string containing path and filename where the testing data should be saved
        recompute: bool
            force to redownload the data and preprocess the data again instead of loading it
        verbose: bool
            print additional scores and timing results where available
        tickers: list[str]
            list containing tickers of interest
        to_csv: bool
            dump active, return, action into a csv file otherwise dump into a pickle file

        clf: XGBClassifier
            contains fitted model after calling fit_classifier
        reg: XGBRegressor
            contains fitted model after calling fit_regressor

    Methods
    -------
        run()
            Processes the data, fits both the classification and regression model; computes the predictions using the
            fitted models before saving them to csv files and printing the obtained scores
        predict()
            Computes the predictions using the fitted classification and regression model
        get_action()
            Computes actions, i.e. -1, 0, 1, limiting trades by the no_action_threshold and no_action_turbulence_threshold
        fit_classifier()
            This method fits the classifier
        fit_regressor()
            This method fits the regressor
        get_train_test()
            Downlaods data from Yahoo Finance if the was no saved data found. Subsequently, the data gets preprocessed
            and split into train and test set.
        scrape_data()
            Downlaods data from Yahoo Finance if the was no saved data found. Subsequently, the data gets preprocessed
            and split into train and test set.
        compute_scraped_features()
            Computes the features according to the provided technical indicator list
        get_bond_indicators()
            Bond rows containing the same date gets flattened into one row
        get_commodity_indicators()
            Commodity rows containing the same date gets flattened into one row
        get_currency_indicators()
            Currency rows containing the same date gets flattened into one row
        get_stock_indicators()
            Stock rows containing the same date gets flattened into one row
    """
    # Assumptions: - all data is available in the morning before trading if not we'd need to us the data from the prior
    #                day to avoid look-ahead bias
    # Thoughts: - Hopelessly overfits to the training data
    #           - Hyperparameter tuning needed (e.g. use the implemented randomised search or opt for Bayesian optimisation instead)
    def __init__(self, dataset_type: str,
                 filename: str,
                 filename_scraped_data: str,
                 filename_indicators: str,
                 filename_raw_indicators: str,
                 filename_output_train: str,
                 filename_output_test: str,
                 recompute: bool,
                 verbose: bool,
                 ticker_list: List[str],
                 to_csv: bool
                 ) -> None:
        super(MLChallenge, self).__init__()
        self.dataset_type = dataset_type
        self.filename = filename
        self.filename_scraped_data = filename_scraped_data
        self.filename_indicators = filename_indicators
        self.filename_raw_indicators = filename_raw_indicators
        self.filename_output_train = filename_output_train
        self.filename_output_test = filename_output_test
        self.recompute = recompute
        self.verbose = verbose
        self.tickers = ticker_list
        self.to_csv = to_csv

        self.clf = None
        self.reg = None

    def run(self) -> None:
        """
        Processes the data, fits both the classification and regression model; computes the predictions using the
        fitted models before saving them to csv files and printing the obtained scores
        """
        # process data
        x_train, y_train, x_test, y_test = self.get_train_test(self.tickers, self.recompute)

        y_train_binary = get_binary_labels(y_train)
        y_test_binary = get_binary_labels(y_test)
        data_train_dict = {'x': x_train, 'y': y_train, 'y_binary': y_train_binary}
        data_test_dict = {'x': x_test, 'y': y_test, 'y_binary': y_test_binary}

        data_train_dict['x_reg'] = data_train_dict['x'][data_train_dict['y_binary'] != 0]
        data_train_dict['y_reg'] = data_train_dict['y'][data_train_dict['y_binary'] != 0]
        data_test_dict['x_reg'] = data_test_dict['x'][data_test_dict['y_binary'] != 0]
        data_test_dict['y_reg'] = data_test_dict['y'][data_test_dict['y_binary'] != 0]

        # fit classifier and regressor
        self.fit_classifier(data_train_dict['x'], data_train_dict['y_binary'], optimise=False, verbose=True)
        self.fit_regressor(data_train_dict['x_reg'], data_train_dict['y_reg'], optimise=False, verbose=True)

        # predict train and test data
        train_pred_active, train_pred_return, train_pred_action = self.predict(data_train_dict['x'])
        test_pred_active, test_pred_return, test_pred_action = self.predict(data_test_dict['x'])

        # save predictions to csv files
        if self.to_csv:
            train_pred_active.to_csv(os.path.join('Data', 'active_' + self.filename_output_train + '.csv'), index=True)
            train_pred_return.to_csv(os.path.join('Data', 'return_' + self.filename_output_train + '.csv'), index=True)
            train_pred_action.to_csv(os.path.join('Data', 'action_' + self.filename_output_train + '.csv'), index=True)
            test_pred_active.to_csv(os.path.join('Data', 'active_' + self.filename_output_test + '.csv'), index=True)
            test_pred_return.to_csv(os.path.join('Data', 'return_' + self.filename_output_test + '.csv'), index=True)
            test_pred_action.to_csv(os.path.join('Data', 'action_' + self.filename_output_test + '.csv'), index=True)
        else:
            # Thoughts: - investigate why we have the duplicates in the first place
            # quick fix: remove duplicate date
            train_pred_active = train_pred_active[~train_pred_active.index.duplicated(keep='first')]
            train_pred_return = train_pred_return[~train_pred_return.index.duplicated(keep='first')]
            train_pred_action = train_pred_action[~train_pred_action.index.duplicated(keep='first')]
            test_pred_active = test_pred_active[~test_pred_active.index.duplicated(keep='first')]
            test_pred_return = test_pred_return[~test_pred_return.index.duplicated(keep='first')]
            test_pred_action = test_pred_action[~test_pred_action.index.duplicated(keep='first')]

            train_result = pd.concat([train_pred_active, train_pred_return, train_pred_action], axis=1)
            test_result = pd.concat([test_pred_active, test_pred_return, test_pred_action], axis=1)
            train_result = train_result.rename(columns={0: 'active', 1: 'return', 2: 'action'})
            test_result = test_result.rename(columns={0: 'active', 1: 'return', 2: 'action'})
            train_result.to_pickle(os.path.join('Data', self.filename_output_train + '.pkl'))
            test_result.to_pickle(os.path.join('Data', self.filename_output_test + '.pkl'))

        # compare result to baseline
        get_scores(data_train_dict, train_pred_active, train_pred_return, train_pred_action,
                   dataset_type='{} train'.format(self.dataset_type))
        get_scores(data_test_dict, test_pred_active, test_pred_return, test_pred_action,
                   dataset_type='{} test'.format(self.dataset_type))

    def predict(self, data: pd.DataFrame) -> Tuple[pd.Series, pd.Series, pd.Series]:
        """
        Computes the predictions using the fitted classification and regression model
        :param data: pandas dataframe containing preprocessed data
        :return: active trading days, return prediction and action prediction on active trading days each as a pandas series
        """
        # predict whether an action was taken
        pred_active = self.clf.predict(data)
        pred_active_series = pd.Series(data=pred_active, index=data.index)

        # Restrict X to where there an action was taken
        x_reg = data[pred_active_series != 0]

        # predict returns at when action was taken
        pred_returns = self.reg.predict(x_reg)
        pred_return_series = pd.Series(data=pred_returns, index=x_reg.index)

        # compute predicted action
        pred_action_series = self.get_action(x_reg, pred_return_series)
        return pred_active_series, pred_return_series, pred_action_series

    @staticmethod
    def get_action(x_data: pd.DataFrame,
                   pred_scores: pd.Series,
                   no_action_threshold_param: float = 0.3,
                   no_action_turbulence_threshold_param: float = 0.95
                   ) -> pd.Series:
        """
        Computes actions, i.e. -1, 0, 1, limiting trades by the no_action_threshold and no_action_turbulence_threshold
        :param x_data: pandas dataframe containing preprocessed data
        :param pred_scores: pandas series containing the predicted scores on the active trading days
        :param no_action_threshold_param: cut of quantile for law absolute predicted returns
        :param no_action_turbulence_threshold_param: cut of quantile for trading days with very high turbulence indicator
        :return: actions according to the return prediction within the no_action and no_action_turbulence threshold
        """
        # Thoughts: - Could use XGBClassifier.predict_proba to get the models uncertainty to trade only at given
        #             threshold that is higher than the default at 50%
        #           - Future extension find a better way to aggregate the turbulence of the scraped data instead of
        #             choosing just one of them

        # PyCharm false-positive warning...
        # Source: https://youtrack.jetbrains.com/issue/PY-43841
        # Accessed: 6/4/21
        action_pred = pred_scores.apply(lambda x: np.sign(x))
        # set weak signals to 'no action' = 0 according to no_action_threshold
        no_action_threshold = np.quantile(pred_scores.abs(), no_action_threshold_param)
        # set 'no action' = 0 according to no_action_turbulence_threshold
        no_action_turbulence_threshold = np.quantile(x_data['turbulence'], no_action_turbulence_threshold_param)
        action_pred = action_pred.apply(lambda x: 0.0 if np.abs(x) < no_action_threshold else x)
        action_pred = action_pred.apply(lambda x: 0.0 if no_action_turbulence_threshold < x else x)

        return action_pred

    def fit_classifier(self, x_data: pd.DataFrame,
                       y_data: pd.Series,
                       param_grid: Dict[str, List[float]] = None,
                       optimise: bool = False,
                       verbose: bool = False
                       ) -> None:
        """
        This method fits the classifier
        :param x_data: pandas dataframe containing preprocessed data
        :param y_data: pandas dataframe contaiining labels
        :param param_grid: if not None, it contains the parameters we would like to optimise over
        :param optimise: if true we run sklearn's randomised search cross validation to find the best parameters
                         according to param_grid
        :param verbose: if both optimise and verbose are True print the randomised searchs results
        """
        # Source: https://machinelearningmastery.com/xgboost-for-imbalanced-classification/
        # Accessed: 3/4/21
        scale_pos_weight = y_data[y_data == 0.0].shape[0] / y_data[y_data == 1.0].shape[0]
        if param_grid is None:
            param_grid = {
                #         'min_child_weight': [1, 5, 10],
                'gamma': [0.5, 1, 1.5],
                #         'subsample': [0.6, 0.8, 1.0],
                #         'colsample_bytree': [0.6, 0.8, 1.0],
                'max_depth': [3, 4, 5]
            }

        if optimise:
            xgb_clf = xgb.XGBClassifier(objective='binary:logistic', nthread=1,
                                        learning_rate=0.1, n_estimators=45, use_label_encoder=False,
                                        scale_pos_weight=scale_pos_weight)
            random_search = RandomizedSearchCV(estimator=xgb_clf, param_distributions=param_grid,
                                               scoring=['balanced_accuracy', 'roc_auc'],
                                               verbose=10, n_jobs=-1, refit='roc_auc', cv=5)
            self.clf = random_search.fit(x_data, y_data)
        else:
            xgb_clf = xgb.XGBClassifier(objective='binary:logistic', nthread=1,
                                        learning_rate=0.1, n_estimators=45, use_label_encoder=False,
                                        scale_pos_weight=scale_pos_weight,
                                        max_depth=3, gamma=0.5)
            self.clf = xgb_clf.fit(x_data, y_data)

        if optimise and verbose:
            print('### classifier randomised search ###')
            means = self.clf.cv_results_['mean_test_balanced_accuracy']
            stds = self.clf.cv_results_['std_test_balanced_accuracy']
            params_ = self.clf.cv_results_['params']
            for mean, stdev, param_ in zip(means, stds, params_):
                print('{} ({}) with {}'.format(mean, stdev, param_))
            print()
            means_roc = self.clf.cv_results_['mean_test_roc_auc']
            stds_roc = self.clf.cv_results_['std_test_roc_auc']
            params_roc = self.clf.cv_results_['params']
            for mean, stdev, param_roc in zip(means_roc, stds_roc, params_roc):
                print('{} ({}) with {}'.format(mean, stdev, param_roc))

    def fit_regressor(self, x_data: pd.DataFrame,
                      y_data: pd.Series,
                      param_grid: Dict[str, List[float]] = None,
                      optimise: bool = False,
                      verbose: bool = False
                      ) -> None:
        """
        This method fits the regressor
        :param x_data: pandas dataframe containing preprocessed data
        :param y_data: pandas dataframe contaiining labels
        :param param_grid: if not None, it contains the parameters we would like to optimise over
        :param optimise: if true we run sklearn's randomised search cross validation to find the best parameters
                         according to param_grid
        :param verbose: if both optimise and verbose are True print the randomised searchs results
        """
        if param_grid is None:
            param_grid = {
                #         'min_child_weight': [1, 5, 10],
                'gamma': [0.5, 1, 1.5],
                #         'subsample': [0.6, 0.8, 1.0],
                #         'colsample_bytree': [0.6, 0.8, 1.0],
                'max_depth': [3, 4, 5]
            }
        if optimise:
            xgb_reg = xgb.XGBRegressor(learning_rate=0.1, n_estimators=45, nthread=1)
            random_search = RandomizedSearchCV(estimator=xgb_reg, param_distributions=param_grid,
                                               scoring='neg_mean_squared_error',
                                               verbose=10, n_jobs=-1, refit=True, cv=5)
            self.reg = random_search.fit(x_data, y_data)
        else:
            xgb_reg = xgb.XGBRegressor(learning_rate=0.1, n_estimators=45, nthread=1,
                                       max_depth=5, gamma=1)
            self.reg = xgb_reg.fit(x_data, y_data)

        if optimise and verbose:
            print('### regressor randomised search ###')
            means_reg = self.reg.cv_results_['mean_test_score']
            stds_reg = self.reg.cv_results_['std_test_score']
            params_reg = self.reg.cv_results_['params']
            for mean, stdev, param_reg in zip(means_reg, stds_reg, params_reg):
                print('{} ({}) with {}'.format(mean, stdev, param_reg))

    ### the following functions that are taking care of scraping and computing the specified indicators ###
    def get_train_test(self, ticker_list: List[str],
                       recompute: bool,
                       split_date: datetime.date = datetime.date(2019, 1, 1),
                       standardise: bool = True
                       ) -> Tuple[pd.DataFrame, pd.Series, pd.DataFrame, pd.Series]:
        """
        Downlaods data from Yahoo Finance if the was no saved data found. Subsequently, the data gets preprocessed
        and split into train and test set.
        :param ticker_list: list containing tickers of interest
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :param split_date: date we use to split the data along into train and test set
        :param standardise: data using sklearn's standard scaler
        :return: two pandas dataframes and two pandas series containing the train test split dataset
        """
        if 'bond' in self.dataset_type:
            flattened_indicators = self.get_bond_indicators(ticker_list, recompute)
        elif 'commodity' in self.dataset_type:
            flattened_indicators = self.get_commodity_indicators(ticker_list, recompute)
            print('dataset_type: {}'.format(self.dataset_type))
        elif 'currency' in self.dataset_type:
            flattened_indicators = self.get_currency_indicators(ticker_list, recompute)
            print('dataset_type: {}'.format(self.dataset_type))
        else:  # 'stock' in self.dataset_type:
            flattened_indicators = self.get_stock_indicators(ticker_list, recompute)
            print('dataset_type: {}'.format(self.dataset_type))
        flattened_indicators = flattened_indicators.set_index('date')
        # Splitting according to task description
        # train & validation: 2006 - 2018
        # test: 2019 - 23/3/2021
        # Thoughts: Since I'm assuming the date to be markovian I'd use sklearn's train_test_split if it
        #           wasn't for the task description
        train = flattened_indicators[flattened_indicators.index < split_date]
        x_train_raw = train.drop('Return', axis=1)
        y_train = train.Return

        test = flattened_indicators[split_date <= flattened_indicators.index]
        x_test_raw = test.drop('Return', axis=1)
        y_test = test.Return

        if standardise:
            x_train_normalised = StandardScaler().fit_transform(x_train_raw.values)
            x_train = pd.DataFrame(x_train_normalised, index=x_train_raw.index, columns=x_train_raw.columns)

            x_test_normalised = StandardScaler().fit_transform(x_test_raw.values)
            x_test = pd.DataFrame(x_test_normalised, index=x_test_raw.index, columns=x_test_raw.columns)

            return x_train, y_train, x_test, y_test

        else:
            return x_train_raw, y_train, x_test_raw, y_test

    @staticmethod
    def scrape_data(filename: str,
                    ticker_list: List[str],
                    recompute: bool,
                    start_date: str = '2006-01-01',
                    end_date: str = '2019-12-31',
                    verbose: bool = False
                    ) -> pd.DataFrame:
        """
        Downlaods data from Yahoo Finance if the was no saved data found. Subsequently, the data gets preprocessed
        and split into train and test set.
        :param filename: describing path to the data directory to either load or save the downloaded data
        :param ticker_list: list containing tickers of interest
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :param start_date: download data starting from (including) this date
        :param end_date: download data ending by (excluding) this date
        :param verbose: print the time it took to download the data
        :return: the scraped data in a pandas dataframe
        """
        ### scrape data ###
        my_file = Path(filename)
        if my_file.is_file() and not recompute:  # file exists
            print('>>> {} exists and was loaded'.format(filename))
            df_scraped = pd.read_pickle(filename)
        else:  # file doesn't exist, scrape data from Yahoo Finance
            if verbose:
                scrape_data_start = time()
            if isinstance(start_date, datetime.date):
                start_date = start_date.strftime('%Y-%m-%d')
            if isinstance(end_date, datetime.date):
                end_date = end_date.strftime('%Y-%m-%d')
            df_scraped = YahooDownloader(start_date=start_date,
                                         end_date=end_date,
                                         ticker_list=ticker_list).fetch_data
            df_scraped.to_pickle(filename)
            if verbose:
                print('elapsed time: {}'.format(time() - scrape_data_start))
        df_scraped['date'] = (pd.to_datetime(df_scraped['date'])).dt.date
        df_scraped = df_scraped.drop(['day'], axis=1)
        return df_scraped

    # feature engineering
    def compute_scraped_features(self, df_scraped: pd.DataFrame,
                                 filename: str,
                                 recompute: bool,
                                 technical_indicator_list: List[str] = TECHNICAL_INDICATORS_LIST,
                                 verbose: bool = False
                                 ) -> pd.DataFrame:
        """
        Computes the features according to the provided technical indicator list
        :param df_scraped: pandas dataframe containing the scraped data
        :param filename: describing path to the data directory to either load or save the downloaded data
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :param technical_indicator_list: list of technical indicators we want to compute
        :param verbose: print the time it took to download the data
        :return: pandas dataframe containing the newly computed indicator features
        """
        my_file = Path(filename)
        if my_file.is_file() and not recompute:  # file exists
            print('>>> {} exists and was loaded'.format(filename))
            processed_scraped = pd.read_pickle(filename)
        else:  # compute scraped features
            fe = FeatureEngineer(use_technical_indicator=True,
                                 tech_indicator_list=technical_indicator_list,
                                 use_turbulence=True,
                                 user_defined_feature=True)

            if verbose:
                compute_features_start = time()
            processed_scraped = fe.preprocess_data(df_scraped, verbose=self.verbose)
            # processed_scraped = processed_scraped.rename(columns={0: 'turbulence'})
            processed_scraped.to_pickle(filename)

            if verbose:
                print('elapsed time for feature engineering: {}'.format(time() - compute_features_start))

        return processed_scraped

    # Thoughts: - Chose to implement get_{}_indicators functions like that to be able to nicely loop in the main
    #             function below
    def get_bond_indicators(self, ticker_list: List[str], recompute: bool) -> pd.DataFrame:
        """
        Rows containing the same date gets flattened into one row
        :param ticker_list: list containing tickers of interest
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :return: pandas dataframe
        """
        my_file = Path(self.filename_indicators)
        if my_file.is_file() and not recompute:  # file exists
            print('>>> {} exists and was loaded'.format(self.filename_indicators))
            bond_flattened_indicators = pd.read_pickle(self.filename_indicators)
        else:  # compute scraped features
            bond = load_given_data(self.filename)
            end_date = bond.date.unique()[-1] + datetime.timedelta(days=1)
            scraped = self.scrape_data(self.filename_scraped_data, ticker_list, recompute, end_date=end_date)
            bond_merged = bond.merge(scraped, on='date', how='outer')
            bond_merged = custom_fillna(bond_merged, self.dataset_type)
            # compute indicators
            bond_indicators = self.compute_scraped_features(bond_merged, filename=self.filename_raw_indicators,
                                                            recompute=recompute)

            # flatten such that there is only one date per line
            bond_flattened_indicators = bond_indicators.drop('Market', axis=1)
            bond_FVX = bond_flattened_indicators[bond_flattened_indicators.tic == '^FVX']
            bond_TNX = bond_flattened_indicators[bond_flattened_indicators.tic == '^TNX']
            bond_TYX = bond_flattened_indicators[bond_flattened_indicators.tic == '^TYX']
            bond_merge1 = bond_FVX.merge(bond_TNX, on=['date', 'Return'],
                                         how='inner', suffixes=['_FVX', '_TNX']).bfill()
            bond_flattened_indicators = bond_merge1.merge(bond_TYX, on=['date', 'Return'],
                                                          how='inner').bfill()
            bond_flattened_indicators = bond_flattened_indicators.drop(['tic_FVX', 'tic_TNX', 'tic'], axis=1)

            # Thoughts: - investigate why we have the duplicates in the first place
            # quick fix: remove duplicate date
            bond_flattened_indicators = bond_flattened_indicators[~bond_flattened_indicators.index.duplicated(keep='first')]
            bond_flattened_indicators.to_pickle(self.filename_indicators)

        return bond_flattened_indicators

    def get_commodity_indicators(self, ticker_list: List[str], recompute: bool) -> pd.DataFrame:
        """
        Rows containing the same date gets flattened into one row
        :param ticker_list: list containing tickers of interest
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :return: pandas dataframe
        """
        my_file = Path(self.filename_indicators)
        if my_file.is_file() and not recompute:  # file exists
            print('>>> {} exists and was loaded'.format(self.filename_indicators))
            flattened_indicators = pd.read_pickle(self.filename_indicators)
        else:  # compute scraped features
            commodity = load_given_data(self.filename)
            end_date = commodity.date.unique()[-1] + datetime.timedelta(days=1)
            scraped = self.scrape_data(self.filename_scraped_data, ticker_list, recompute, end_date=end_date)
            commodity_merged = commodity.merge(scraped, on='date', how='outer')
            commodity_merged = custom_fillna(commodity_merged, self.dataset_type)
            # compute indicators
            commodity_indicators = self.compute_scraped_features(commodity_merged,
                                                                 filename=self.filename_raw_indicators,
                                                                 recompute=recompute)

            # flatten such that there is only one date per line
            flattened_indicators = commodity_indicators.drop('Market', axis=1)
            commodity_CLF = flattened_indicators[flattened_indicators.tic == 'CL=F']
            commodity_GCF = flattened_indicators[flattened_indicators.tic == 'GC=F']
            flattened_indicators = commodity_CLF.merge(commodity_GCF, on=['date', 'Return'],
                                                       how='inner', suffixes=['_CL=F', '']).bfill()
            flattened_indicators = flattened_indicators.drop(['tic_CL=F', 'tic'], axis=1)

            # Thoughts: - investigate why we have the duplicates in the first place
            # quick fix: remove duplicate date
            flattened_indicators = flattened_indicators[~flattened_indicators.index.duplicated(keep='first')]
            flattened_indicators.to_pickle(self.filename_indicators)

        return flattened_indicators

    def get_currency_indicators(self, ticker_list: List[str], recompute: bool) -> pd.DataFrame:
        """
        Rows containing the same date gets flattened into one row
        :param ticker_list: list containing tickers of interest
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :return: pandas dataframe
        """
        my_file = Path(self.filename_indicators)
        if my_file.is_file() and not recompute:  # file exists
            print('>>> {} exists and was loaded'.format(self.filename_indicators))
            currency_flattened_indicators = pd.read_pickle(self.filename_indicators)
        else:  # compute scraped features
            currency = load_given_data(self.filename)
            end_date = currency.date.unique()[-1] + datetime.timedelta(days=1)
            scraped = self.scrape_data(self.filename_scraped_data, ticker_list, recompute, end_date=end_date)
            currency_merged = currency.merge(scraped, on='date', how='outer')
            currency_merged = custom_fillna(currency_merged, self.dataset_type)
            # compute indicators
            currency_indicators = self.compute_scraped_features(currency_merged, filename=self.filename_raw_indicators,
                                                                recompute=recompute)

            # flatten such that there is only one date per line
            currency_flattened_indicators = currency_indicators.drop('Market', axis=1)
            currency_6EF = currency_flattened_indicators[currency_flattened_indicators.tic == '6E=F']
            currency_GEF = currency_flattened_indicators[currency_flattened_indicators.tic == 'GE=F']
            currency_6JF = currency_flattened_indicators[currency_flattened_indicators.tic == '6J=F']
            currency_merge1 = currency_6EF.merge(currency_GEF, on=['date', 'Return'],
                                                 how='inner', suffixes=['_6E=F', '_GE=F']).bfill()
            currency_flattened_indicators = currency_merge1.merge(currency_6JF, on=['date', 'Return'],
                                                                  how='inner').bfill()
            currency_flattened_indicators = currency_flattened_indicators.drop(['tic_6E=F', 'tic_GE=F', 'tic'], axis=1)

            # Thoughts: - investigate why we have the duplicates in the first place
            # quick fix: remove duplicate date
            currency_flattened_indicators = currency_flattened_indicators[~currency_flattened_indicators.index.duplicated(keep='first')]
            currency_flattened_indicators.to_pickle(self.filename_indicators)

        return currency_flattened_indicators

    def get_stock_indicators(self, ticker_list: List[str], recompute: bool) -> pd.DataFrame:
        """
        Rows containing the same date gets flattened into one row
        :param ticker_list: list containing tickers of interest
        :param recompute: force to redownload the data and preprocess the data again instead of loading it
        :return: pandas dataframe
        """
        my_file = Path(self.filename_indicators)
        if my_file.is_file() and not recompute:  # file exists
            print('>>> {} exists and was loaded'.format(self.filename_indicators))
            flattened_indicators = pd.read_pickle(self.filename_indicators)
        else:  # compute scraped features
            stock = load_given_data(self.filename)
            end_date = stock.date.unique()[-1] + datetime.timedelta(days=1)
            scraped = self.scrape_data(self.filename_scraped_data, ticker_list, recompute, end_date=end_date)
            stock_merged = stock.merge(scraped, on='date', how='outer')
            stock_merged = custom_fillna(stock_merged, self.dataset_type)
            # compute indicators
            stock_indicators = self.compute_scraped_features(stock_merged, filename=self.filename_raw_indicators,
                                                             recompute=recompute)

            # flatten such that there is only one date per line
            flattened_indicators = stock_indicators.drop(['Market', 'tic'], axis=1)

            # Thoughts: - investigate why we have the duplicates in the first place
            # quick fix: remove duplicate date
            flattened_indicators = flattened_indicators[~flattened_indicators.index.duplicated(keep='first')]
            flattened_indicators.to_pickle(self.filename_indicators)

        return flattened_indicators


if __name__ == '__main__':
    start = time()
    tickers_dict = {'bond': ['^TNX', '^FVX', '^TYX'],
                    'commodity': ['CL=F', 'GC=F'],
                    'currency': ['6E=F', 'GE=F', '6J=F'],
                    'stock': ['ES=F']}

    for asset_type, tickers in tickers_dict.items():
        for i in range(1, 3):
            print('### running {}{} ###'.format(asset_type, i))
            parameters = {'dataset_type': '{}{}'.format(asset_type, i),
                          'filename': 'Data/{}_{}.csv'.format(asset_type, i),
                          'filename_scraped_data': 'Data/ml_scraped_{}_data.pkl'.format(asset_type),
                          'filename_indicators': 'Data/ml_{}{}_indicators.pkl'.format(asset_type, i),
                          'filename_raw_indicators': 'Data/ml_{}{}_raw_indicators.pkl'.format(asset_type, i),
                          'filename_output_train': 'train_{}{}_prediction'.format(asset_type, i),
                          'filename_output_test': 'test_{}{}_prediction'.format(asset_type, i),
                          'recompute': True,
                          'ticker_list': tickers,
                          'verbose': False,
                          'to_csv': False}
            Model_ML = MLChallenge(**parameters)
            Model_ML.run()
            print('\n\n')

    print('##### total time elapsed: {}'.format(time() - start))
